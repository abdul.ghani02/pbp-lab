import 'package:flutter/material.dart';

import '../screens/filters_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Colors.white,//Theme.of(context).accentColor,
            child: Text(
              'MENU',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Colors.black87),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('HOME', Icons.home, () {
            //Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('ARTIKEL', Icons.book, () {
            //Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
          buildListTile('BED', Icons.bed, () {
            //Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
          buildListTile('VAKSIN', Icons.medication, () {
            //Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
          buildListTile('REGISTER', Icons.app_registration_rounded, () {
            //Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
          buildListTile('LOGIN', Icons.person, () {
            //Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
        ],
      ),
    );
  }
}