1. Apakah perbedaan antara JSON dan XML?
Jawab: Berdasarkan seperti lab 2 yang menggunakan kedua format tersebut, perbedaannya adalah sebagai berikut: 
- JSON menggunakan meta-language, sedangkan XML menggunakan markup-language.
- JSON biasanya digunakan untuk mengolah data, sedangkan XML untuk mengolah dokumen dan data transfer.
- JSON lebih mudah dibaca ketimbang XML yang berisi kode-kode yang rumit dan sulit dibaca oleh mata manusia.
- Ekstensi file JSON diakhiri dengan '.json', sedangkan XML adalah '.xml'.


2. Apakah perbedaan antara HTML dan XML?
Jawab: Perbedaan yang mendasari kedua format kode tersebut ialah:
- HTML biasanya digunakan untuk mengolah tampilan data, sementara XML mengolah data transfernya.
- HTML menggunakan case insensitive, sementara XML adalah case sensitive.
- Penggunaan tag penutup bersifat wajib pada XML, sementara pada HTML ada yang tidak ada yang harus.