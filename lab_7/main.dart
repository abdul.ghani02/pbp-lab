import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Edit Profile",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiCheckBox1 = false;
  bool nilaiSwitch = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Profile"),
        backgroundColor: Colors.black87,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "contoh: RS Segar Alam",
                      labelText: "Nama Rumah Sakit",
                      icon: Icon(Icons.business_sharp),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      labelText: "Lokasi",
                      icon: Icon(Icons.add_location_sharp),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Lokasi tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "contoh: +62 xxx xxxx xxxx",
                      labelText: "Nomor Telepon",
                      icon: Icon(Icons.add_ic_call),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nomor telepon tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                CheckboxListTile(
                  title: Text('Ketersediaan Vaksin'),
                  subtitle: Text('Inventori rumah sakit'),
                  value: nilaiCheckBox,
                  activeColor: Colors.blueGrey,
                  onChanged: (value) {
                    setState(() {
                      nilaiCheckBox = value!;
                    });
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      labelText: "Jumlah Vaksin Tersedia",
                      hintText: "0",
                      icon: Icon(Icons.colorize),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Tidak ada vaksin yang tersedia';
                      }
                      return null;
                    },
                  ),
                ),
                CheckboxListTile(
                  title: Text('Ketersediaan Bed'),
                  subtitle: Text('Bed yang masih kosong'),
                  value: nilaiCheckBox1,
                  activeColor: Colors.blueGrey,
                  onChanged: (value) {
                    setState(() {
                      nilaiCheckBox1 = value!;
                    });
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      labelText: "Jumlah Bed Tersedia",
                      hintText: "0",
                      icon: Icon(Icons.airline_seat_individual_suite),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Tidak ada bed yang tersedia';
                      }
                      return null;
                    },
                  ),
                ),
                SwitchListTile(
                  title: Text('Kerumunan di Rumah Sakit'),
                  subtitle: Text('Gunakan slider berikut untuk memperkirakan'),
                  value: nilaiSwitch,
                  activeTrackColor: Colors.grey,
                  activeColor: Colors.blueGrey,
                  onChanged: (value) {
                    setState(() {
                      nilaiSwitch = value;
                    });
                  },
                ),
                Slider(
                  value: nilaiSlider,
                  min: 0,
                  max: 100,
                  activeColor: Colors.blueGrey,
                  inactiveColor: Colors.grey,
                  onChanged: (value) {
                    setState(() {
                      nilaiSlider = value;
                    });
                  },
                ),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blueAccent,
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                          title: const Text("Profil berhasil diubah"),
                          content: Padding(
                          padding: const EdgeInsets.all(10.0),
                          )
                          )
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}